# -*- coding: utf-8 -*-

import random as rn
import networkx as nx
import matplotlib.pyplot as plt
import copy

class game:
    
    def __init__(self,matrix):
        self.matrix = matrix
        self.players = len(matrix)
        self.p = [1/self.players for i in range(self.players)]
        self.q = [0.5 for i in range(2)]

    def show(self):
        print('============GAME============')
        print(self.players,' players')
        print()
        for i in range(self.players):
            print(i,': ',self.matrix[i])
        print()
        p_string = 'p = [ '
        accuracy = 4
        for i in range(self.players):
            p_string += str(round(self.p[i], accuracy))
            if i != (self.players - 1): 
                p_string += ', '
        print(p_string,']')
        print('q = [ ' + str(round(self.q[0],accuracy)) + ', ' + str(round(self.q[1],accuracy)) + ' ]')
        print('============END=============')
    
    def edge_add(self,i,j):
        if i != j:
            self.matrix[i][j] = 1
            self.matrix[j][i] = 1
    
    def edge_del(self,i,j):
        self.matrix[i][j] = 0
        self.matrix[j][i] = 0
    
    def d_i(self,i):
        return sum(self.matrix[i])
    
    def d(self):
        temp = []
        for i in range(self.players):
            temp.append( self.d_i(i) )
        return temp
    
    def d_unique(self):
        temp = self.d()
        temp = list(set(temp))
        temp.sort()
        return temp

    def N_i(self,i):
        temp = []
        for j in range(self.players):
            if self.matrix[i][j] == 1:
                temp.append( j )
        return temp
    
    def N(self):
        temp = {i: self.N_i(i) for i in range(self.players)}
        return temp
    
    def Z(self):
        #without degree = 0
        temp = {}
        d = self.d()
        d_unique = self.d_unique()
        if d_unique == [0]:
            return {0: [i for i in range(self.players)]}
        if d_unique == [self.players-1]:
            return {self.players-1: [i for i in range(self.players)]}
        for i in range(len(d_unique)):
            if d_unique[i] == 0:
                continue
            temp[d_unique[i]] = []
            for j in range(len(d)):
                if d_unique[i] == d[j]:
                    temp[d_unique[i]].append( j )
        return temp
    
    def createEmpty(size):
        matrix = []
        for i in range(size):
            matrix.append( [0 for j in range(size)] )
        return game(matrix)
        
    def createFull(size):
        matrix = []
        for i in range(size):
            matrix.append( [1 for j in range(size)] )
            matrix[i][i] = 0                
        return game(matrix)
        
    def createNSG(size,vector):
        tempGame = game.createEmpty(size)
        for i in range(len(vector)):
            if vector[i] == 1:
                for j in range(i):
                    tempGame.edge_add(i,j)                
        return tempGame
   
    def generateRandom(size):
        tempGame = game.createEmpty(size)
        for i in range(tempGame.players):
            for j in range(i):
                temp = rn.randint(0,1)
                if temp == 1:
                    tempGame.edge_add(i,j)
        return tempGame

    def generateRandomNSG(size,probability=0.5):
        #probability of creating a dominant vertex
        #default is 0.5
        vectorVertice = [0]
        for i in range(1,size):
            prob = rn.uniform(0,1)
            if prob >= probability:
                vectorVertice.append(0)
            else:
                vectorVertice.append(1)
        print(probability)
        return game.createNSG(size,vectorVertice)
 
    def checkNSGnew(self): 
        #return 0 if NSG
        d = self.d()
        Z = self.Z()
        d_unique = list(Z.keys())
        D_plus = len(d_unique)        
        if D_plus == 0:
            print('empty')
            return 0
        if ( D_plus == 1 )and( d[0] == self.players-1 ):
            print('full')
            return 0
        d_range = copy.deepcopy( d_unique )
        d_range.sort()
        D = [0 for i in range(D_plus)]
        for i in range(D_plus):
            j = d_unique.index(d_range[i])
            D[j] = i+1
        del d_range
        u = [] #gamer i in Z_j
        v = [] #gamers in {Z_1,...,Z_(sizeZ+1-j)}
        for i in range(D_plus):
            u = Z[d_unique[i]]
            v = []
            j = D[d_unique[i]]
            for k in range(1,D_plus+1-j):
                v.extend( Z[k] )
            v = list(set(v))
            for k in u:
                count = 0
                for j in v:
                    if k == j:
                        continue
                    else:
                        count += 1
                    if self.matrix[k][j] != self.matrix[k][j]:
                        return 1
                    if self.matrix[k][j] == 0:
                        return 1
                if count != d[k]:
                    return 1
        return 0
    
    def checkNSG(self): #return 0 if NSG
        n = self.players
        matrix = self.matrix
        all_degrees = []      
        unique_degrees = []
        D = {}
        for i in range(n):
            all_degrees.append( sum(matrix[i]) )
            if all_degrees[i] == 0: 
                continue
            try:
                D[all_degrees[i]].append( i )
            except KeyError:
                D[all_degrees[i]] = []
                D[all_degrees[i]].append( i )                
        unique_degrees = list(set(all_degrees))
        unique_degrees.sort()
        if unique_degrees[0] == 0:
            unique_degrees = unique_degrees[1:]
        unique_degrees.reverse()
        D_plus = len(unique_degrees)
        if unique_degrees == []:
            return 0
        if (len(unique_degrees) == 1)and(unique_degrees[0] == n-1):
            return 0
        for i in range(len(unique_degrees)):
            u = []
            for j in range(0,D_plus-i):
                u.extend( D[unique_degrees[j]] )
            v = D[unique_degrees[i]]
            for k in v:
                count = 0
                for j in u:
                    if j == k:
                        continue
                    else: 
                        count += 1
                    if matrix[j][k] != matrix[k][j]:
                        return 1                    
                    if matrix[j][k] == 0:
                        return 1
                if count != all_degrees[k]:
                    return 1
        return 0
    
    def checkNSGmy(self):
        n = self.players
        matrix = copy.deepcopy(self.matrix)
        vector = []
        full_vector = []
        full_vector_label = []
        max_degrees = 0
        count = 0  
        while len(matrix)>0:
            if len(matrix) == count:
                return [1]          
            count = n = len(matrix)
            for i in range(n-1,-1,-1):
                if sum(matrix[i]) == 0:
                    vector.append(i)
                    matrix.pop(i)
            for i in range(len(matrix)):
                for k in vector:
                    matrix[i].pop(k)
            full_vector.extend(vector)
            for i in range(len(vector)):
                full_vector_label.append(0)
            vector = []  
            
            n = len(matrix)
            max_degrees = n-1
            for i in range(n-1,-1,-1):
                if sum(matrix[i]) == max_degrees:
                    vector.append(i)
                    matrix.pop(i)
            for i in range(len(matrix)):
                for k in vector:
                    matrix[i].pop(k)
            full_vector.extend(vector)
            for i in range(len(vector)):
                full_vector_label.append(1)
            vector = []
        full_vector_label.append(0)
        full_vector_label.reverse()
        return full_vector_label


    def random_proc(self):
        n = self.players
        matrix = self.matrix
        p = rn.randint(0,self.players-1)
        q = rn.randint(0,1)
        if q == 0:
            m = []
            degrees = []
            gamers = []
            for i in range(n):
                if i == p: continue;
                if matrix[i][p] == 1:
                    m.append( i )
                    degrees.append( sum(matrix[i]) )
            if len(m) == 0:
                print('Player ',p,' must remove the link, but he cant')
                return 0
            min_d = min(degrees)
            for i in range(len(degrees)):
                if degrees[i] == min_d:
                    gamers.append(m[i])
            gamer = gamers[0]
            #gamer = degrees.index(min_d)
            self.matrix[p][gamer] = self.matrix[gamer][p] = 0
            print('Player ',p,' remove the link with ',gamer,' player')
        else:
            M = []
            degrees = []
            gamers = []
            for i in range(n):
                if i == p: continue;
                if matrix[i][p] == 0:
                    M.append( i )
                    degrees.append( sum(matrix[i]) )
            if len(M) == 0:
                print('Player ',p,' must create the link, but he cant')
                return 0
            max_d = max(degrees)
            for i in range(len(degrees)):
                if degrees[i] == max_d:
                    gamers.append(M[i])
            gamer = gamers[0]
            #gamer = degrees.index(max_d)
            self.matrix[p][gamer] = self.matrix[gamer][p] = 1
            print('Player ',p,' create the link with ',gamer,' player')

    def show_graph(self):
        temp_g = nx.Graph()
        temp_list = []
        for i in range(self.players):
            temp_g.add_node(i)
            for j in range(self.players):
                if self.matrix[i][j] != 0:
                    temp_list.append( (i,j,self.matrix[i][j]) )
        temp_g.add_weighted_edges_from(temp_list)       
        plt.figure(3,figsize=(6,4)) 
        nx.draw_circular(temp_g, with_labels=True, node_color=[0.8,0.8,0.8])
        plt.show()
        return temp_g
        
for i in range(1000):
    game1 = game.generateRandom(7)
    temp1 = game1.checkNSG()
    temp2 = game1.checkNSGnew()
    if temp1 != temp2: 
        print('alert')

#print(temp1)
#print(temp2)

n = 7
step = 0

game1 = game.generateRandom(n)

times = 0;
for i in range(step):
    game1.show_graph()
    result = game1.checkNSG()
    if result == 0:
        print('graph is NSG')
    else: 
        print('graph is not NSG')
        times += 1
    game1.random_proc()
    
print(times,'iteration from',i+1)